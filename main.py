from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from pymongo import MongoClient
from bson import ObjectId
from bson import json_util
import json

# Definición de la clase Alumno para validar el cuerpo de las solicitudes
class Alumno(BaseModel):
    nombre: str
    edad: str
    curso: str

app = FastAPI()

# Conexión a la base de datos MongoDB
client = MongoClient('mongodb://localhost:27017/')
db = client['escuela']
alumnos_collection = db['alumnos']

# Operaciones CRUD

# Read
@app.get("/socket.io/")
async def obtener_alumno():
    # Buscar el alumno por su ID en la colección de MongoDB
    raise HTTPException(status_code=404, detail="Ruta no encontrado")
       

# Create
@app.post("/alumnos/")
async def crear_alumno(alumno: Alumno):
    alumno_dict = alumno.dict()
    # Insertar el alumno en la colección de MongoDB
    resultado = alumnos_collection.insert_one(alumno_dict)
    # Devolver el ID del documento recién insertado
    return {"mensaje": "Alumno creado exitosamente", "id_alumno": str(resultado.inserted_id)}

# Read
@app.get("/alumnos/{alumno_id}")
async def obtener_alumno(alumno_id: str):
    # Buscar el alumno por su ID en la colección de MongoDB
    alumno = alumnos_collection.find_one({"_id": ObjectId(alumno_id)})
    print(alumno)
    if alumno:
        return json.loads(json_util.dumps(alumno))
    else:
        raise HTTPException(status_code=404, detail="Alumno no encontrado")

# Update
@app.put("/alumnos/{alumno_id}")
async def actualizar_alumno(alumno_id: str, alumno: Alumno):
    # Convertir los datos del alumno a un diccionario
    alumno_dict = alumno.dict()
    # Actualizar el alumno en la colección de MongoDB
    resultado = alumnos_collection.update_one({"_id": ObjectId(alumno_id)}, {"$set": alumno_dict})
    if resultado.modified_count == 1:
        return {"mensaje": "Alumno actualizado exitosamente"}
    else:
        raise HTTPException(status_code=404, detail="Alumno no encontrado")

# Delete
@app.delete("/alumnos/{alumno_id}")
async def eliminar_alumno(alumno_id: str):
    # Eliminar el alumno por su ID de la colección de MongoDB
    resultado = alumnos_collection.delete_one({"_id": ObjectId(alumno_id)})
    if resultado.deleted_count == 1:
        return {"mensaje": "Alumno eliminado exitosamente"}
    else:
        raise HTTPException(status_code=404, detail="Alumno no encontrado")
